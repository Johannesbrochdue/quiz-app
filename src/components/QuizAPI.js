// Api that delivers methods to retrieve quiz-questions and quiz-categories

export const QuizAPI = {
	async fetchQuestions(difficulty, amount, category) {
		try {
			return await fetch(
				`https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}`
			)
				.then((response) => response.json())
				.then((response) => response.results);
		} catch (error) {
			console.log("failed to fetch questions", error.message);
		}
	},
	async fetchCategories() {
		try {
			return await fetch(`https://opentdb.com/api_category.php`)
				.then((response) => response.json())
				.then((res) => res.trivia_categories);
		} catch (e) {
			console.log(e.message);
		}
	},
};
