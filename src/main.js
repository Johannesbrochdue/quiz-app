import Vue from "vue";
import store from "./store";
import vSelect from "vue-select";
import router from "@/router";
import App from "./App.vue";

Vue.config.productionTip = false;
Vue.component("v-select", vSelect);

new Vue({
	router,
	store,
	render: (h) => h(App),
}).$mount("#app");
